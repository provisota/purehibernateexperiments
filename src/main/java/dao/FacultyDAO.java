package dao;

import domain.Faculty;
import util.Constants.FACULTY;

/**
 * @author Alterovych Ilya
 *         <p>
 *         Interface for describe specific DAO methods
 *         (that can't describe in Generic DAO) for Faculty entity only.
 */
public interface FacultyDAO extends GenericDAO<Faculty, Long> {
    Faculty getFacultyByName(FACULTY name);
}
