package dao.impl;

import dao.AbstractGenericDAO;
import dao.StudentDAO;
import domain.Student;

/**
 * @author Alterovych Ilya
 */
public class StudentDAOImpl extends AbstractGenericDAO<Student, Long>
        implements StudentDAO {
    public StudentDAOImpl() {
        super(Student.class);
    }
}