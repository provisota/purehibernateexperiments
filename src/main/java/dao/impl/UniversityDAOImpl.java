package dao.impl;

import dao.AbstractGenericDAO;
import dao.UniversityDAO;
import domain.University;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import util.Constants.UNIVERSITY;
import util.HibernateUtil;

import java.util.List;

/**
 * @author Alterovych Ilya
 */
public class UniversityDAOImpl extends AbstractGenericDAO<University, Long>
        implements UniversityDAO {
    public UniversityDAOImpl() {
        super(University.class);
    }

    private static final Logger log = Logger.getLogger(UniversityDAOImpl.class);

    @Override
    public University getUniversityByName(UNIVERSITY name) {
        Session session = null;
        University university = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(University.class);
            criteria.add(Restrictions.eq("name", name));
            criteria.setMaxResults(1);
            List result = criteria.list();
            if (!result.isEmpty()) {
                university = (University) result.get(0);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return university;
    }
}
