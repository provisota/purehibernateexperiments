package dao.impl;

import dao.AbstractGenericDAO;
import dao.FacultyDAO;
import domain.Faculty;
import domain.Student;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import util.Constants;
import util.HibernateUtil;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Alterovych Ilya
 */
public class FacultyDAOImpl extends AbstractGenericDAO<Faculty, Long>
        implements FacultyDAO {
    private static final Logger log = Logger.getLogger(FacultyDAOImpl.class);

    public FacultyDAOImpl() {
        super(Faculty.class);
    }

    @Override
    public Faculty getFacultyByName(Constants.FACULTY name) {
        Session session = null;
        Faculty faculty = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Criteria criteria = session.createCriteria(Faculty.class);
            criteria.add(Restrictions.eq("name", name));
            criteria.setMaxResults(1);
            List result = criteria.list();
            if (!result.isEmpty()) {
                faculty = (Faculty) result.get(0);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }

        return faculty;
    }

    @Override
    public void deleteEntity(Faculty faculty) throws SQLException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            faculty = (Faculty) session.load(Faculty.class, faculty.getId());
            for (Student student : faculty.getStudents()) {
                student.setFaculty(null);
            }
            session.beginTransaction();
            session.merge(faculty);
            session.getTransaction().commit();
        } catch (Exception e) {
            log.error(e.getMessage());
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        super.deleteEntity(faculty);
    }
}
