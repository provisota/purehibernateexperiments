package dao;

import domain.University;
import util.Constants.UNIVERSITY;

/**
 * @author Alterovych Ilya
 *         <p/>
 *         Interface for describe specific DAO methods
 *         (that can't describe in Generic DAO) for University entity only.
 */
public interface UniversityDAO extends GenericDAO<University, Long> {
    University getUniversityByName(UNIVERSITY name);
}
