package dao;

import domain.Student;

/**
 * @author Alterovych Ilya
 *         <p/>
 *         Interface for describe specific DAO methods
 *         (that can't describe in Generic DAO) for Student entity only.
 */
public interface StudentDAO extends GenericDAO<Student, Long> {
}