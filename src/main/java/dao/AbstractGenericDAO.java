package dao;

import domain.BaseEntity;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import util.HibernateUtil;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Alterovych Ilya
 */
public abstract class AbstractGenericDAO<T extends BaseEntity, PK extends Serializable>
        implements GenericDAO<T, PK> {
    private Class<T> persistentClass;
    private static final Logger log = Logger.getLogger(AbstractGenericDAO.class);

    public AbstractGenericDAO(Class<T> persistentClass) {
        this.persistentClass = persistentClass;
    }

    @Override
    public void deleteAllEntities() throws SQLException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            String query = "delete from " +
                    this.persistentClass.getSimpleName();
            session.createQuery(query).executeUpdate();
            session.getTransaction().commit();
        } catch (Exception e) {
            log.error(e.getMessage());
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public void deleteAllEntitiesWithChildren() throws SQLException {
        List<T> entities = getAllEntity();
        if (entities.size() > 0) {
            for (T entity : entities) {
                deleteEntity(entity);
            }
        }
    }

    @Override
    public void saveEntity(T entity) throws SQLException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(entity);
            session.getTransaction().commit();
        } catch (Exception e) {
            log.error(e.getMessage());
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @Override
    public void saveAll(List<T> entities) throws SQLException {
        for (T entity : entities) {
            saveEntity(entity);
        }
    }

    @Override
    public void updateEntity(T entity) throws SQLException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(entity);
            session.getTransaction().commit();
        } catch (Exception e) {
            log.error(e.getMessage());
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public T mergeEntity(T entity) throws SQLException {
        Session session = null;
        T result = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            result = (T) session.merge(entity);
            session.getTransaction().commit();
        } catch (Exception e) {
            log.error(e.getMessage());
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return result;
    }

    @Override
    public List<T> mergeAll(List<T> entities) throws SQLException {
        List<T> mergedList = new ArrayList<T>();
        for (T entity : entities) {
            T mergedEntity = mergeEntity(entity);
            if (mergedEntity != null) {
                mergedList.add(mergedEntity);
            }
        }
        return mergedList;
    }

    @SuppressWarnings("unchecked")
    @Override
    public T getEntityById(PK id) throws SQLException {
        Session session = null;
        T entity = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            entity = (T) session.get(this.persistentClass, id);
        } catch (Exception e) {
            log.error(e.getMessage());
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return entity;
    }

    @SuppressWarnings("unchecked")
    @Override
    public T loadEntityById(PK id) throws SQLException {
        Session session = null;
        T entity = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            entity = (T) session.load(this.persistentClass, id);
        } catch (Exception e) {
            log.error(e.getMessage());
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        return entity;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<T> getAllEntity() throws SQLException {

        Session session = null;
        List<T> entities = new ArrayList<T>();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            entities = session.createCriteria(this.persistentClass).list();
        } catch (Exception e) {
            log.error(e.getMessage());
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
        System.out.println("Get entities list from DB size: " + entities.size());
        return entities;
    }

    @Override
    public void deleteEntity(T entity) throws SQLException {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(entity);
            session.getTransaction().commit();
        } catch (Exception e) {
            log.error(e.getMessage());
            log.error("Fail to delete entity: " + entity.getClass().getSimpleName()
                    + ", id ='" + entity.getId() + '\'');
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }
}
