package dao;

import dao.impl.FacultyDAOImpl;
import dao.impl.StudentDAOImpl;
import dao.impl.UniversityDAOImpl;

/**
 * @author Alterovych Ilya
 */
public class Factory {

    private static StudentDAO studentDAO = null;
    private static UniversityDAO universityDAO = null;
    private static FacultyDAO facultyDAO = null;

    private static Factory instance = null;

    public static synchronized Factory getInstance() {
        if (instance == null) {
            instance = new Factory();
        }
        return instance;
    }

    public StudentDAO getStudentDAO() {
        if (studentDAO == null) {
            studentDAO = new StudentDAOImpl();
        }
        return studentDAO;
    }

    public UniversityDAO getUniversityDAO() {
        if (universityDAO == null) {
            universityDAO = new UniversityDAOImpl();
        }
        return universityDAO;
    }

    public FacultyDAO getFacultyDAO() {
        if (facultyDAO == null) {
            facultyDAO = new FacultyDAOImpl();
        }
        return facultyDAO;
    }
}