package dao;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Alterovych Ilya
 */
public interface GenericDAO<T, PK extends Serializable> {
    void saveEntity(T entity) throws SQLException;

    void saveAll(List<T> entities) throws SQLException;

    void updateEntity(T entity) throws SQLException;

    T mergeEntity(T entity) throws SQLException;

    List<T> mergeAll(List<T> entities) throws SQLException;

    T getEntityById(PK id) throws SQLException;

    T loadEntityById(PK id) throws SQLException;

    List<T> getAllEntity() throws SQLException;

    void deleteEntity(T entity) throws SQLException;

    void deleteAllEntities() throws SQLException;

    void deleteAllEntitiesWithChildren() throws SQLException;
}
