package domain;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import util.Constants.CITY;
import util.Constants.UNIVERSITY;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Alterovych Ilya
 */
@Entity
@Table(name = "university")
public class University extends BaseEntity{
    private static final long serialVersionUID = -4274619516812756984L;

    private UNIVERSITY name;
    private String address;
    private Integer capacity;
    private CITY city;
    private List<Faculty> faculties = new ArrayList<Faculty>();
    private List<Student> students = new ArrayList<Student>();

    public University() {
    }

    public University(UNIVERSITY name, String address, Integer capacity, CITY city) {
        this.name = name;
        this.address = address;
        this.capacity = capacity;
        this.city = city;
    }

    @Column(name = "name", unique = true, nullable = false)
    @Enumerated(EnumType.STRING)
    public UNIVERSITY getName() {
        return name;
    }

    public void setName(UNIVERSITY name) {
        this.name = name;
    }

    @Column(name = "address", nullable = false)
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Column(name = "capacity", nullable = false)
    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    @Column(name = "city", nullable = false)
    public CITY getCity() {
        return city;
    }

    public void setCity(CITY city) {
        this.city = city;
    }

    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.ALL}, orphanRemoval = true
            , mappedBy = "university")
    @Fetch(FetchMode.SELECT)
    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.ALL}, orphanRemoval = true
            , mappedBy = "university")
    @Fetch(FetchMode.SELECT)
    public List<Faculty> getFaculties() {
        return faculties;
    }

    public void addFaculty(Faculty faculty) {
        faculty.setUniversity(this);
        getFaculties().add(faculty);
    }

    public void setFaculties(List<Faculty> faculties) {
        this.faculties = faculties;
    }

    @Override
    public String toString() {
        return "University{" +
                "id='" + getId() + '\'' +
                ", name='" + getName().getLabel() + '\'' +
                ", address='" + getAddress() + '\'' +
                ", city='" + getCity().getLabel() + '\'' +
                ", capacity=" + getCapacity() + "\n" +
                ", faculties=" + Arrays.toString(getFaculties().toArray()) + "\n" +
                ", students=" + Arrays.toString(getStudents().toArray()) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        University that = (University) o;

        if (!address.equals(that.address)) return false;
        if (!capacity.equals(that.capacity)) return false;
        if (getId() != null ? !getId().equals(that.getId())
                : that.getId() != null) return false;
        if (city != that.city) return false;
        if (name != that.name) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + city.hashCode();
        result = 31 * result + address.hashCode();
        result = 31 * result + capacity.hashCode();
        return result;
    }
}
