package domain;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import util.Constants.FACULTY;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Alterovych Ilya
 */
@Entity
@Table(name = "faculty")
public class Faculty extends BaseEntity {
    private static final long serialVersionUID = 406957000909363983L;

    private FACULTY name;
    private Integer capacity;
    private University university;
    private List<Student> students = new ArrayList<Student>();

    public Faculty() {
    }

    public Faculty(FACULTY name, Integer capacity) {
        this.name = name;
        this.capacity = capacity;
    }

    @Column(name = "name", nullable = false, unique = true)
    public FACULTY getName() {
        return name;
    }

    public void setName(FACULTY name) {
        this.name = name;
    }

    @Column(name = "capacity", nullable = false)
    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST,
            CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "university_id", nullable = false)
    public University getUniversity() {
        return university;
    }

    public void setUniversity(University university) {
        this.university = university;
    }

    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST,
            CascadeType.MERGE, CascadeType.REFRESH}, mappedBy = "faculty")
    @Fetch(FetchMode.SELECT)
    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Faculty{");
        sb.append("id=").append(id);
        sb.append(", name='").append(getName().getLabel()).append('\'');
        sb.append(", capacity=").append(capacity);
        sb.append(", university='").append(getUniversity().getName().getLabel()).append('\'');
        sb.append(", students count=").append(getStudents().size());
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Faculty faculty = (Faculty) o;

        if (getId() != null ? !getId().equals(faculty.getId()) : faculty.getId() != null)
            return false;
        if (getName() != faculty.getName()) return false;
        if (!getCapacity().equals(faculty.getCapacity())) return false;
        return getUniversity().equals(faculty.getUniversity());

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + getName().hashCode();
        result = 31 * result + getCapacity().hashCode();
        result = 31 * result + getUniversity().hashCode();
        return result;
    }
}
