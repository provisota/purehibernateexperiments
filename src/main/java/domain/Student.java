package domain;

/**
 * @author Alterovych Ilya
 */

import util.Constants.*;

import javax.persistence.*;

@Entity
@Table(name="student")
public class Student extends BaseEntity{

    private static final long serialVersionUID = 3622050666074924082L;
    private String name;
    private Long age;
    private SEX sex;
    private University university;
    private Faculty faculty;

    public Student(){
    }

    public Student(String name, Long age, SEX sex, University university) {
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.university = university;
    }

    public Student(String name, Long age, SEX sex, University university, Faculty faculty) {
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.university = university;
        this.faculty = faculty;
    }

    @Column(name = "name", nullable = false)
    public String getName(){
        return name;
    }

    @Column(name = "age", nullable = false)
    public Long getAge(){
        return age;
    }

    @Column(name = "sex", nullable = false)
    @Enumerated(value = EnumType.STRING)
    public SEX getSex() {
        return sex;
    }

    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "university_id", nullable = false)
    public University getUniversity() {
        return university;
    }

    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "faculty_id", nullable = true)
    public Faculty getFaculty() {
        return faculty;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public void setSex(SEX sex) {
        this.sex = sex;
    }

    public void setUniversity(University university) {
        this.university = university;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + getId() +
                ", name='" + getName() + '\'' +
                ", age=" + getAge() +
                ", sex='" + getSex().getLabel() + '\'' +
                ", faculty='" + (getFaculty() == null ? "null"
                : getFaculty().getName().getLabel()) + '\'' +
                ", university='" + (getUniversity().getName().getLabel()) + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Student student = (Student) o;

        if (age != null ? !age.equals(student.age) : student.age != null) return false;
        if (sex != null ? !sex.equals(student.sex) : student.sex != null) return false;
        if (getId() != null ? !getId().equals(student.getId())
                : student.getId() != null) return false;
        if (name != null ? !name.equals(student.name) : student.name != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (age != null ? age.hashCode() : 0);
        result = 31 * result + (sex != null ? sex.hashCode() : 0);
        return result;
    }
}