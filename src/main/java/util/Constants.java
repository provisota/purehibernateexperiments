package util;

/**
 * @author Alterovych Ilya
 */
public class Constants {
    public enum UNIVERSITY {
        REFRIGERATION("Холодильный"),

        LAWYERS("Юридическая Академия");

        private String label;

        UNIVERSITY(String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }
    }

    public enum SEX {
        MALE("Мужчина"),

        FEMALE("Женщина");

        private String label;

        SEX(String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }
    }

    public enum FACULTY {
        MATHEMATICS("Математика"),

        PHYSICS("Физика"),

        LAW("Право"),

        ECONOMICS("Экономика");

        private String label;

        FACULTY(String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }
    }

    public enum CITY {
        ODESSA("Одесса"),

        KIEV("Киев");

        private String label;

        CITY(String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }
    }
}
