package main;

/**
 * @author Alterovych Ilya
 */

import dao.Factory;
import dao.FacultyDAO;
import dao.StudentDAO;
import dao.UniversityDAO;
import domain.Faculty;
import domain.Student;
import domain.University;
import org.apache.log4j.Logger;
import util.Constants.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Main {
    private static StudentDAO studentDAO = Factory.getInstance().getStudentDAO();
    private static UniversityDAO universityDAO = Factory.getInstance().getUniversityDAO();
    private static FacultyDAO facultyDAO = Factory.getInstance().getFacultyDAO();

    private static final Logger logger = Logger.getLogger(Main.class);

    public static void main(String[] args) {
        try {
            deleteAllData();

            addTestUniversities();

            addTestFaculties();

//            addTestStudentsWithoutFaculty();

            addTestStudentsWithFaculty();
            facultyDAO.deleteEntity(facultyDAO.getFacultyByName(FACULTY.ECONOMICS));
            facultyDAO.deleteEntity(facultyDAO.getFacultyByName(FACULTY.MATHEMATICS));

            printResults();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.exit(0);
    }

    private static void deleteAllData() throws SQLException{
        universityDAO.deleteAllEntitiesWithChildren();
    }

    private static void addTestUniversities() throws SQLException {
        List<University> universities = new ArrayList<University>();

        if (universityDAO.getUniversityByName(UNIVERSITY.LAWYERS) == null) {
            universities.add(new University(UNIVERSITY.LAWYERS,
                    "ул. Сергея Варламова 7", 5000, CITY.KIEV));
        }
        if (universityDAO.getUniversityByName(UNIVERSITY.REFRIGERATION) == null) {
            universities.add(new University(UNIVERSITY.REFRIGERATION,
                    "ул. Дворянская 1/3", 3000, CITY.ODESSA));
        }
        if (universities.size() > 0) {
            universityDAO.mergeAll(universities);
        }
    }

    private static void addTestFaculties() throws SQLException {
        List<University> universities = new ArrayList<University>();

        University lawyersAcademy
                = universityDAO.getUniversityByName(UNIVERSITY.LAWYERS);
        University refrigerationAcademy
                = universityDAO.getUniversityByName(UNIVERSITY.REFRIGERATION);
        universities.add(lawyersAcademy);
        universities.add(refrigerationAcademy);

        lawyersAcademy.addFaculty(new Faculty(FACULTY.LAW, 150));
        lawyersAcademy.addFaculty(new Faculty(FACULTY.ECONOMICS, 125));

        refrigerationAcademy.addFaculty(new Faculty(FACULTY.MATHEMATICS, 200));
        refrigerationAcademy.addFaculty(new Faculty(FACULTY.PHYSICS, 220));

        universityDAO.mergeAll(universities);
    }

    private static void addTestStudentsWithFaculty() throws SQLException {
        University lawyersAcademy
                = universityDAO.getUniversityByName(UNIVERSITY.LAWYERS);
        University refrigerationAcademy
                = universityDAO.getUniversityByName(UNIVERSITY.REFRIGERATION);

        Faculty mathFac = facultyDAO.getFacultyByName(FACULTY.MATHEMATICS);
        Faculty physFac = facultyDAO.getFacultyByName(FACULTY.PHYSICS);
        Faculty lawFac = facultyDAO.getFacultyByName(FACULTY.LAW);
        Faculty economyFac = facultyDAO.getFacultyByName(FACULTY.ECONOMICS);

        List<Student> students = new ArrayList<Student>();

        students.add(new Student("Петя Васичкин", 21l, SEX.MALE,
                lawyersAcademy, lawFac));
        students.add(new Student("Алиса Пертрова", 24l, SEX.FEMALE,
                lawyersAcademy, lawFac));
        students.add(new Student("Фёдор Гаврюхин", 17l, SEX.MALE,
                lawyersAcademy, economyFac));
        students.add(new Student("Наташа Поперечная", 32l, SEX.FEMALE,
                lawyersAcademy, economyFac));

        students.add(new Student("Вася Петечкин", 17l, SEX.MALE,
                refrigerationAcademy, mathFac));
        students.add(new Student("Ольга Селезнёва", 22l, SEX.FEMALE,
                refrigerationAcademy, mathFac));
        students.add(new Student("Толик Ноликов", 19l, SEX.MALE,
                refrigerationAcademy, physFac));
        students.add(new Student("Сара Работник", 21l, SEX.FEMALE,
                refrigerationAcademy, physFac));

        studentDAO.mergeAll(students);
    }

    private static void addTestStudentsWithoutFaculty() throws SQLException {
        University lawyersAcademy
                = universityDAO.getUniversityByName(UNIVERSITY.LAWYERS);
        University refrigerationAcademy
                = universityDAO.getUniversityByName(UNIVERSITY.REFRIGERATION);
        List<Student> students = new ArrayList<Student>();

        students.add(new Student("Петя Васичкин", 21l, SEX.MALE, lawyersAcademy));
        students.add(new Student("Алиса Пертрова", 24l, SEX.FEMALE, lawyersAcademy));
        students.add(new Student("Фёдор Гаврюхин", 17l, SEX.MALE, lawyersAcademy));
        students.add(new Student("Наташа Поперечная", 32l, SEX.FEMALE, lawyersAcademy));

        students.add(new Student("Вася Петечкин", 17l, SEX.MALE, refrigerationAcademy));
        students.add(new Student("Ольга Селезнёва", 22l, SEX.FEMALE, refrigerationAcademy));
        students.add(new Student("Толик Ноликов", 19l, SEX.MALE, refrigerationAcademy));
        students.add(new Student("Сара Работник", 21l, SEX.FEMALE, refrigerationAcademy));

        studentDAO.mergeAll(students);
    }

    private static void printResults() throws SQLException {
        printStudents();
        printUniversities();
        printFaculties();
    }

    private static void printUniversities() throws SQLException {
        List<University> universities = universityDAO.getAllEntity();
        System.out.println("=======Все университеты======");
        for (University university : universities) {
            System.out.println(university);
        }
        System.out.println("=============================");
    }

    private static void printStudents() throws SQLException {
        List<Student> students = studentDAO.getAllEntity();
        System.out.println("========Все студенты=========");
        for (Student student : students) {
            System.out.println(student);
        }
        System.out.println("=============================");
    }

    private static void printFaculties() throws SQLException {
        List<Faculty> faculties = facultyDAO.getAllEntity();
        System.out.println("========Все факультеты=========");
        for (Faculty faculty : faculties) {
            System.out.println(faculty);
        }
        System.out.println("=============================");
    }
}